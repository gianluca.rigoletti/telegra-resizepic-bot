const TeleBot = require("telebot")
const outdent = require("outdent")
const jimp = require("jimp")
var fs = require("fs")
require("dotenv").config()
const bot = new TeleBot(process.env.TOKEN)
const usersStepMap = new Map()
const usersPictureMap = new Map()
const picturesSet = new Set()
const telegramUploadUrl = `https://api.telegram.org/file/bot${process.env
  .TOKEN}/`

bot.on("/start", msg => {
  usersStepMap.set(msg.chat.id, 1)
  const reply = outdent`
    This bot lets you resize a picture.
    Start by uploading a picture
  `
  bot.sendMessage(msg.chat.id, reply)
})

bot.on("photo", async msg => {
  usersStepMap.set(msg.chat.id, 2)
  const fileId = msg.photo[msg.photo.length - 1].file_id
  const { width, height } = msg.photo[msg.photo.length - 1]
  picturesSet.add(msg.photo[msg.photo.length - 1].file_id)
  const reply = outdent`
  Great! The picture has the dimensions of \`${width} ${height}\`
   Now write the width and the height you want to resize your picture to.
  Put a space between the width and the height. A valid examples are:
  \`512 512\`
  \`640 320\`
  \`320 640\`
  `
  const file = await bot.getFile(fileId)
  usersPictureMap.set(msg.chat.id, file)
  bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on("text", async msg => {
  if (usersStepMap.get(msg.chat.id) === 2) {
    const dimensions = parseWidthAndHeight(msg.text)
    const { file_path, file_id } = usersPictureMap.get(msg.chat.id)
    if (fs.existsSync(`./img/${file_id}.png`)) {
      return bot.sendPhoto(msg.chat.id, file_id)
    }
    const fileLink = `${telegramUploadUrl}${file_path}`
    bot.sendAction(msg.chat.id, "upload_document")
    console.log(fileLink)
    await jimp
      .read(fileLink)
      .then(image => {
        image
          .resize(dimensions[0], dimensions[1], jimp.RESIZE_NEAREST_NEIGHBOR)
          .write(`./img/${file_id}.png`, () => {
            bot
              .sendDocument(msg.chat.id, `./img/${file_id}.png`)
              .then(result => {
                picturesSet.add(
                  result.result.photo[result.result.photo.length - 1].file_id
                )
              })
          })
      })
      .catch(err => {
        console.log(err)
      })
  }
})

const parseWidthAndHeight = text => {
  const trimmedString = text.trim()
  try {
    return trimmedString.split(" ").map(el => parseInt(el))
  } catch (error) {
    console.log(error)
    return [null, null]
  }
}

process.on("unhandledRejection", (reason, p) => {
  console.log("Unhandled Rejection at: Promise", p, "reason:", reason)
})

bot.start()
